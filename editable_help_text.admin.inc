<?php

//@TODO: Access function to check that get params exists

function _editable_help_text_required_get_params(){
  return array(
    'entity_type',
    'bundle',
    'field_name',
  );
}

function _editable_help_text_get_info($field_name, $entity_type, $bundle){

  if($field_name == 'title'){
    $entity_info = entity_get_info('node');
    $field_info_widget_type = field_info_widget_types('text_textfield');
    $node_type_info = node_type_load($bundle);

    return array(
      'field' => array(
        'label' => $node_type_info->title_label,
        'bundles' => array('node' => array_keys($entity_info['bundles'])),
      ),
      'widget' => array(
        'type' => 'text_textfield',
        'label' => $field_info_widget_type['label'],
      ),
    );

  }

  $field_info = field_info_field($field_name);
  $field_info_instance = field_info_instance($entity_type, $field_name, $bundle);
  $field_info_widget_type = field_info_widget_types($field_info_instance['widget']['type']);

  $info = array(
    'field' => array(
      'label' => $field_info_instance['label'],
      'bundles' => $field_info['bundles'],
    ),
    'widget' => array(
      'type' => $field_info_instance['widget']['type'],
      'label' => $field_info_widget_type['label'],
    ),
  );

  return $info;
}

function _editable_help_text_admin_settings_form(){

  $editable_help_texts = variable_get('editable_help_texts', array());
  $config_field = isset($editable_help_texts['field'][$_GET['field_name']]) ? $editable_help_texts['field'][$_GET['field_name']] : array();

  //@TODO: Make this query work (use db_or()) ?
  $query = db_select('editable_help_text', 'e');
  $query->fields('e', array('value', 'format', 'use_on'));
  $query->leftJoin('editable_help_text_bundles', 'eb', 'e.hid = eb.hid AND eb.bundle = :bundle', array(':bundle' => $_GET['bundle']));
  $query->condition('e.type', 'field');
//    ->orderBy('uid', 'ASC')
  $a = $query->execute();

  foreach($a as $b){
    $c =1;
  }

  $a = 1;

  if(
    isset($config_field['use_on']) && (
      ( $config_field['use_on'] == 'some' && empty($config_field['instances']['node'][$_GET['bundle']]) )
    )
  ){
    $config_field = array();
  }

  $field_help_text_info = _editable_help_text_get_info($_GET['field_name'], $_GET['entity_type'], $_GET['bundle']);

  $form['field'] = array(
    '#type' => 'fieldset',
    '#title' => t('Help text for the !label field', array('!label' => '<em>' . $field_help_text_info['field']['label'] . '</em>')),
    '#tree' => TRUE,
  );

  $form['field']['help_text'] = array(
    '#type' => 'text_format',
    '#default_value' => isset($config_field['help_text']['value']) ? $config_field['help_text']['value'] : '',
    '#format' => isset($config_field['help_text']['format']) ? $config_field['help_text']['format'] : filter_default_format(),
  );


  if(count($field_help_text_info['field']['bundles']) == 1 && isset($field_help_text_info['field']['bundles']['node'])){
    $bundles = $field_help_text_info['field']['bundles']['node'];
    $entity_info = entity_get_info('node');

    foreach($bundles as $bundle){
      $bundles_human[$bundle] = $entity_info['bundles'][$bundle]['label'];
    }
  }

  $form['field']['use_on'] = array(
    '#title' => t('Use this helptext..'),
    '#type' => 'radios',
    '#options' => array(
      'all' => t('..on all content types where this field exists (currently on !content_types).', array('!content_types' => '<em>' . implode('</em>, <em>', $bundles_human) . '</em>')),
      'some' => t('..on selected content types.'),
    ),
    '#default_value' => isset($config_field['use_on']) ? $config_field['use_on'] : 'all',
    '#description' => t('When you choose to use on selected content types, available content types will appear.'),
  );

  if(count($field_help_text_info['field']['bundles']) == 1 && isset($field_help_text_info['field']['bundles']['node'])){

    $form['field']['instances']['node'] = array(
      '#title' => t('Content types'),
      '#type' => 'checkboxes',
      '#options' => $bundles_human,
      '#default_value' => isset($config_field['instances']['node']) ? $config_field['instances']['node'] : array(),
      '#description' => t('Content types where the <em>@label</em> field uses this help text</label>', array('@label' => $field_help_text_info['field']['label'])),
      '#tree' => TRUE,
      '#states' => array(
        'visible' => array(
          ':input[name="field[use_on]"]' => array('value' => 'some'),
        ),
      ),
    );

  }


  if(!empty($editable_help_texts['widget'][$field_help_text_info['widget']['type']])){
    $widget_values = $editable_help_texts['widget'][$field_help_text_info['widget']['type']];
  }

  $form['widget_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add help text for all fields using the <em>@widget</em> input type', array('@widget' => $field_help_text_info['widget']['label'])),
    '#description' => t('Usually this is for instructions on how to use that input type.'),
    '#default_value' => !empty($widget_values['widget_enabled']),
  );

  $form['widget'] = array(
    '#type' => 'fieldset',
    '#title' => t('Help text for all !widget fields', array('!widget' => '<em>' . $field_help_text_info['widget']['label'] . '</em>')),
    '#states' => array(
      'visible' => array(
        'input[name=widget_enabled]' => array('checked' => TRUE),
      ),
    ),
    '#tree' => TRUE,
  );

  $form['widget']['help_text'] = array(
    '#description' => t('This help text will be shown on all fields that use the same input type as the field <em>@label</em> field', array('@label' => $field_help_text_info['field']['label'])),
    '#type' => 'text_format',
    '#default_value' => isset($widget_values['help_text']['value']) ? $widget_values['help_text']['value'] : '',
    '#format' => isset($widget_values['help_text']['format']) ? $widget_values['help_text']['format'] : filter_default_format(),
  );

  $form['widget_type'] = array(
    '#type' => 'value',
    '#value' => $field_help_text_info['widget']['type'],
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save help text'));

  $form['#submit'][] = '_editable_help_text_admin_settings_form_submit';

  foreach(_editable_help_text_required_get_params() as $param) {
    $form['params'][$param] = array(
      '#type' => 'value',
      '#value' => $_GET[$param],
    );
  }

  return $form;
}

function _editable_help_text_admin_settings_form_submit($form, &$form_state){
  $editable_help_texts = variable_get('editable_help_texts', array());
  $values = $form_state['values'];

  if(!empty($values['field']['help_text']['value'])){

    $hid = db_insert('editable_help_text')
      ->fields(array(
        'type' => 'field',
        'use_on' => $values['field']['use_on'],
        'value' => $values['field']['help_text']['value'],
        'format' => $values['field']['help_text']['format'],
      ))
      ->execute();

    if($values['field']['use_on'] == 'some'){
      foreach($values['field']['instances'] as $entity_type => $bundles){
        foreach($bundles as $bundle => $value){
          if($value){
            db_insert('editable_help_text_bundles')
              ->fields(array(
                'hid' => $hid,
                'entity_type' => $entity_type,
                'bundle' => $bundle,
              ))
              ->execute();
          }
        }
      }
    }
  }

  $editable_help_texts['field'][$values['field_name']] = $values['field'];

  if(!empty($values['widget_enabled'])){
    $editable_help_texts['widget'][$values['widget_type']] = $values['widget'];
  }
  $editable_help_texts['widget'][$values['widget_type']]['widget_enabled'] = $values['widget_enabled'];

  variable_set('editable_help_texts', $editable_help_texts);
}